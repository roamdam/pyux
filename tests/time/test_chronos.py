
import unittest
from unittest.mock import patch
import os
from io import StringIO

from time import sleep
from pyux.time import Chronos
from pyux.errors import NoDurationsError
from tests.time import THIS_FOLDER


class TestChronos(unittest.TestCase):

    def test_repr(self):
        chrono = Chronos()
        chrono.lap('One')
        chrono.lap('Two')
        self.assertEqual(repr(chrono), 'Chronos(laps = start, One, Two)')

    def test_lap_name(self):
        chrono = Chronos()
        sleep(0.02)
        chrono.lap('step 1')
        self.assertEqual(chrono.lap_names[0], 'start')
        self.assertEqual(chrono.lap_names[1], 'step 1')

    def test_lap_time(self):
        chrono = Chronos()
        sleep(0.1)
        chrono.lap('step 1')
        self.assertLess(
            abs(chrono.lap_times[1] - chrono.lap_times[0] - 0.1), 0.01)

    def test_duration(self):
        chrono = Chronos()
        sleep(0.1)
        chrono.lap('step 1')
        sleep(0.4)
        chrono.lap('step 2')
        chrono.compute_durations()
        self.assertLess(abs(chrono.durations['step 1'] - 0.1), 0.01)
        self.assertLess(abs(chrono.durations['step 2'] - 0.4), 0.01)

    def test_duration_ms(self):
        chrono = Chronos()
        sleep(0.2)
        chrono.lap('step 1')

        chrono.compute_durations(ms=True)
        self.assertLess(abs(chrono.durations['step 1'] - 200), 10)

    def test_duration_nonms(self):
        chrono = Chronos()
        sleep(0.2)
        chrono.lap('step 1')
        chrono.compute_durations(ms=False)
        self.assertLess(abs(chrono.durations['step 1'] - 0.2), 0.01)

    def test_total_duration(self):
        chrono = Chronos()
        sleep(0.5)
        chrono.lap('step 1')
        chrono.compute_durations(ms=False)
        self.assertLess(abs(chrono.durations['total'] - 0.5), 0.01)


class TestChronosTSV(unittest.TestCase):
    def setUp(self) -> None:
        self.test_file = os.path.join(THIS_FOLDER, 'times.tsv')

    def tearDown(self) -> None:
        if os.path.exists(self.test_file):
            os.unlink(self.test_file)

    def test_tsv_empty_durations(self):
        chrono = Chronos()
        with self.assertRaises(NoDurationsError):
            chrono.write_tsv(run_name='unittests', path=self.test_file)

    def test_tsv_none_columns(self):
        chrono = Chronos()
        sleep(0.5)
        chrono.lap('test lap').compute_durations()

        chrono.write_tsv(run_name='unittests', path=self.test_file)
        with open(self.test_file, 'r') as file:
            res = file.read().splitlines(keepends=False)

        self.assertEqual(res[0], 'Execution\tStep\tDuration (secs)')

    def test_tsv_custom_columns(self):
        chrono = Chronos()
        sleep(0.5)
        chrono.lap('test lap').compute_durations()

        chrono.write_tsv(run_name='unittests', path=self.test_file,
                         col_names=('one', 'two', 'three'))
        with open(self.test_file, 'r') as file:
            res = file.read().splitlines(keepends=False)

        self.assertEqual(res[0], 'one\ttwo\tthree')

    def test_tsv_wrong_columns(self):
        chrono = Chronos()\
            .lap('test lap')\
            .compute_durations()

        with self.assertRaises(ValueError):
            chrono.write_tsv(run_name='unittests',
                             path=self.test_file, col_names=('hi', 'bye'))

    def test_tsv_append(self):
        chrono = Chronos().lap('test lap').compute_durations()
        target_str = '\n'.join((
            'Execution\tStep\tDuration (secs)',
            'test\ttest lap\t0.0',
            'test\ttotal\t0.0',
            'test2\ttest lap\t0.0',
            'test2\ttotal\t0.0\n'
        ))
        chrono.write_tsv(run_name='test', path=self.test_file)
        chrono.write_tsv(run_name='test2', path=self.test_file)

        with open(self.test_file, 'r') as file:
            res = file.read()

        self.assertEqual(res, target_str)


class TestChronosLoop(unittest.TestCase):

    def setUp(self) -> None:
        self.path = os.path.join(THIS_FOLDER, 'times.tsv')

    def tearDown(self) -> None:
        if os.path.exists(self.path):
            os.unlink(self.path)

    @patch('sys.stdout', new_callable=StringIO)
    def test_simple(self, mock):
        iterable = range(2)
        for _ in Chronos(iterable, console_print=True):
            sleep(1)

        target_str = '\n'.join((
            '\nStep\tDuration',
            "iteration 0\t1",
            "iteration 1\t1",
            "total\t2\n"
        ))

        self.assertEqual(mock.getvalue(), target_str)

    def test_write(self):
        iterable = range(5)
        for _ in Chronos(iterable, ms=True, write_tsv=True, path=self.path):
            pass

        self.assertTrue(os.path.exists(self.path))
