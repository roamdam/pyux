import unittest
from unittest.mock import patch
import time

from pyux.errors import TimeoutThreadError
from pyux.time import timeout


class TestTimeout(unittest.TestCase):

    def test_raise_func_exception(self):
        @timeout(5)
        def raise_error():
            raise ValueError('A value error')

        with(self.assertRaises(ValueError)):
            raise_error()

    @patch('pyux.time.Thread.start')
    def test_thread_error(self, mock_thread):
        @timeout(5)
        def return_one():
            return 1

        mock_thread.side_effect = ValueError
        with(self.assertRaises(TimeoutThreadError)):
            return_one()

    def test_timeout_doesnt_stop_function(self):
        delay = 3
        duration = 2
        @timeout(delay=delay)
        def time_sleep(x):
            time.sleep(x)
            return True

        result = time_sleep(x=duration)

        self.assertTrue(result)

    def test_timeout_stops_function(self):
        delay = 2
        duration = 3
        @timeout(delay)
        def time_sleep(x):
            time.sleep(x)
            return True

        with self.assertRaises(TimeoutError):
            time_sleep(x=duration)
