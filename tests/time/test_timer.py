
import unittest

from timeit import default_timer
from unittest.mock import patch
from io import StringIO
from pyux.time import Timer
from pyux.errors import DelayTypeError


class TestTimer(unittest.TestCase):

    @patch('sys.stdout', new_callable=StringIO)
    def test_timer_null(self, mock_stdout):
        Timer(delay=0, message='hello')
        self.assertEqual(mock_stdout.getvalue(), '\r0 | hello\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_timer_downward(self, mock_stdout):
        target_str = '\r1 | hello' + '\r0 | hello\n'

        Timer(delay=1, message='hello')

        self.assertEqual(mock_stdout.getvalue(), target_str)

    def test_no_delay_error(self):
        with self.assertRaises(DelayTypeError):
            Timer()

    @patch('sys.stdout', new_callable=StringIO)
    def test_overwrite(self, mock_stdout):
        target_str = '\r0 | a' + '\r0 | b'

        for letter in ('a', 'b'):
            Timer(delay=0, message=letter, overwrite=True)

        self.assertEqual(mock_stdout.getvalue(), target_str)

    @patch('sys.stdout')
    def test_delay(self, mock_stdout):
        _ = mock_stdout
        start = default_timer()
        Timer(delay=1, message='nothing')
        stop = default_timer()

        self.assertLess(abs(start - stop), 1.01)


class TestTimerLoop(unittest.TestCase):
    @patch('sys.stdout', new_callable=StringIO)
    def test_index_loop(self, mock):
        iterable = range(2)
        target_str = ''.join((
            '\r1 | 0', '\r0 | 0',
            '\r1 | 1', '\r0 | 1\n'
        ))
        for _ in Timer(iterable, delay=1, ms=True):
            pass

        self.assertEqual(mock.getvalue(), target_str)

    @patch('sys.stdout', new_callable=StringIO)
    def test_value_loop(self, mock):
        target_str = ''.join((
            '\r1 | a', '\r0 | a',
            '\r1 | b', '\r0 | b\n'
        ))
        for _ in Timer('ab', delay=1, print_value=True, ms=True):
            pass

        self.assertEqual(mock.getvalue(), target_str)

    @patch('sys.stdout')
    def test_range_loop(self, _):
        full_range = Timer(range(3), delay=1, ms=True)
        short_range = Timer(3, delay=1, ms=True)

        self.assertEqual(full_range.iterable, short_range.iterable)

    @patch('sys.stdout', new_callable=StringIO)
    def test_generator(self, mock):
        target_str = ''.join((
            '\r1 | a', '\r0 | a',
            '\r1 | b', '\r0 | b\n'
        ))

        def generator():
            yield 'a'
            yield 'b'

        for _ in Timer(generator(), delay=1, print_value=True):
            pass

        self.assertEqual(mock.getvalue(), target_str)
