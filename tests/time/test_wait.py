
import unittest
from unittest.mock import patch
from timeit import default_timer

from pyux.time import wait


class TestWait(unittest.TestCase):

    def test_before(self):
        @wait(delay=0.2)
        def hello():
            return

        start = default_timer()
        hello()
        stop = default_timer()

        self.assertLess(abs(start - stop), 0.22)

    def test_after(self):
        @wait(delay=0.2, before=False)
        def hello():
            return

        start = default_timer()
        hello()
        stop = default_timer()

        self.assertLess(abs(start - stop), 0.22)

    def test_return_value(self):
        @wait(delay=0.1)
        def one():
            return "one"

        res = one()
        self.assertEqual(res, "one")

    @staticmethod
    @patch('pyux.time.Timer')
    def test_with_timer(mock_timer):
        @wait(delay=1, timer=True, ms=True)
        def one():
            return "one"

        one()
        mock_timer.assert_called_once_with(delay=1, iterable=None, ms=True)
