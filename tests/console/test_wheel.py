import unittest
from unittest.mock import patch
from io import StringIO
from pyux.console import Wheel


class TestWheel(unittest.TestCase):

    def test_non_iterable_error(self):
        with self.assertRaises(TypeError):
            Wheel(iterable=Wheel)

    @patch('sys.stdout', new_callable=StringIO)
    def test_print(self, mock_stdout):
        progress = Wheel(range(4))

        progress.print(step=0)

        self.assertEqual(mock_stdout.getvalue(), '\r - | 0')

    def test_int_arg(self):
        wheel_range = Wheel(range(10))
        wheel_int = Wheel(10)

        self.assertEqual(wheel_range.iterable, wheel_int.iterable)

    @patch('sys.stdout', new_callable=StringIO)
    def test_wheel_iterator(self, mock_stdout):
        for step in Wheel(4):
            _ = step

        target_str = "%s%s%s%s%s" % (
            '\r - | 0',
            '\r \\ | 1',
            '\r | | 2',
            '\r / | 3',
            '\r - | 4\n'
        )
        self.assertEqual(mock_stdout.getvalue(), target_str)

    @patch('sys.stdout', new_callable=StringIO)
    def test_wheel_generator(self, mock_stdout):
        def test_generator():
            yield 'A'
            yield 'B'
            yield 'C'

        for _ in Wheel(test_generator(), print_value=True):
            pass

        target_str = "%s%s%s%s" % (
            '\r - | A',
            '\r \\ | B',
            '\r | | C',
            '\r / | C\n'
        )
        self.assertEqual(mock_stdout.getvalue(), target_str)

    @patch('sys.stdout', new_callable=StringIO)
    def test_wheel_value(self, mock_stdout):
        for _ in Wheel('ABCD', print_value=True):
            pass

        target_str = '%s%s%s%s%s' % (
            '\r - | A',
            '\r \\ | B',
            '\r | | C',
            '\r / | D',
            '\r - | D\n'
        )

        self.assertEqual(mock_stdout.getvalue(), target_str)

    @patch('sys.stdout', new_callable=StringIO)
    def test_wheel_manual(self, mock_stdout):
        iterable = 'ABCD'
        target_str = '%s%s%s%s%s' % (
            '\r - | 0',
            '\r \\ | 1',
            '\r | | 2',
            '\r / | 3',
            '\r - | 4\n'
        )
        wheel = Wheel(iterable)

        for counter, _ in enumerate(iterable):
            wheel.print(step=counter)
        wheel.close()

        self.assertEqual(mock_stdout.getvalue(), target_str)

    @patch('sys.stdout', new_callable=StringIO)
    def test_wheel_manual_message(self, mock_stdout):
        iterable = 'ABC'
        target_str = '%s%s%s%s' % (
            '\r - | A0',
            '\r \\ | B1',
            '\r | | C2',
            '\r / | ABC\n'
        )
        wheel = Wheel(iterable)

        for counter, letter in enumerate(iterable):
            wheel.print(step=counter, message='%s%d' % (letter, counter))
        wheel.close(message='ABC')

        self.assertEqual(mock_stdout.getvalue(), target_str)

    @patch('sys.stdout', new_callable=StringIO)
    def test_close(self, mock_stdout):
        wheel = Wheel(4)

        wheel.close()

        self.assertEqual(mock_stdout.getvalue(), '\r - | 4\n')
