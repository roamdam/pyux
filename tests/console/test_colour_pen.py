
import unittest
import sys
from io import StringIO
from unittest.mock import patch
from colorama import Fore, Style
from colorama import deinit

from pyux.console import ColourPen
from pyux.errors import ColorValueError
from pyux.errors import StyleValueError


class TestColourPenInners(unittest.TestCase):

    def setUp(self) -> None:
        self.pen = ColourPen()
        self.pen.message = 'hello'
        sys.stdout.write(Style.RESET_ALL + '')
        sys.stdout.flush()

    def tearDown(self) -> None:
        sys.stdout.write(Style.RESET_ALL + '')
        sys.stdout.flush()

    def test_color_message(self):
        self.pen._colourise(color='RED')

        self.assertEqual(self.pen.message, Fore.RED + 'hello')

    def test_color_default(self):
        self.pen._colourise()

        self.assertEqual(self.pen.message, Fore.RESET + 'hello')

    def test_color_none_preserve(self):
        self.pen._colourise(color='red')

        self.pen._colourise(color=None)

        self.assertEqual(self.pen.message, Fore.RED + 'hello')

    def test_color_error(self):
        with self.assertRaises(ColorValueError):
            self.pen._colourise(color='ORANGE')

    def test_style_message(self):
        self.pen._style(style='BRIGHT')

        self.assertEqual(self.pen.message, Style.BRIGHT + 'hello')

    def test_style_default(self):
        self.pen._style()

        self.assertEqual(self.pen.message, Style.RESET_ALL + 'hello')

    def test_style_none_preserve(self):
        self.pen._style(style='bright')

        self.pen._style(style=None)

        self.assertEqual(self.pen.message, Style.BRIGHT + 'hello')

    def test_style_error(self):
        with self.assertRaises(StyleValueError):
            self.pen._style(style='BOLD')


class TestColourPenWrite(unittest.TestCase):

    def setUp(self) -> None:
        self.pen = ColourPen()
        deinit()

    def test_repr(self):
        self.pen.message = 'hello'
        self.assertEqual(repr(self.pen), "ColourPen(message = hello)")

    @patch('sys.stdout', new_callable=StringIO)
    def test_write_standard(self, mock_stdout):
        self.pen.write('hello', color='red', style='bright')

        self.assertEqual(mock_stdout.getvalue(),
                         Style.BRIGHT + Fore.RED + 'hello')

    @patch('sys.stdout', new_callable=StringIO)
    def test_write_newline(self, mock_stdout):
        self.pen.write('hello', color='red', style='bright', newline=True)

        self.assertEqual(mock_stdout.getvalue(),
                         Style.BRIGHT + Fore.RED + 'hello' + '\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_write_continue(self, mock_stdout):
        self.pen\
            .write('hello', color='red', style='bright')\
            .write('bye')

        self.assertEqual(mock_stdout.getvalue(),
                         Style.BRIGHT + Fore.RED + 'hello' + 'bye')

    @patch('sys.stdout', new_callable=StringIO)
    def test_write_reset(self, mock_stdout):
        self.pen.write('hello', color='red', style='bright', reset=True)

        self.assertEqual(mock_stdout.getvalue(), Style.BRIGHT +
                         Fore.RED + 'hello' + Style.RESET_ALL)

    @patch('sys.stdout')
    def test_flush(self, mock_stdout):
        self.pen.write('hello', flush=True)
        self.pen.write('hello', flush=False)

        mock_stdout.flush.assert_called_once_with()

    @patch('sys.stdout', new_callable=StringIO)
    def test_close(self, mock_stdout):
        target_str = 'Another one bites the dust, ah.' + Style.RESET_ALL + '\n'

        self.pen.write('Another one bites the dust, ah.').close()

        self.assertEqual(mock_stdout.getvalue(), target_str)
