
import unittest
from unittest.mock import patch
from io import StringIO
from pyux.console import ProgressBar


class TestProgressBar(unittest.TestCase):

    def test_non_iterable_error(self):
        with self.assertRaises(TypeError):
            ProgressBar(iterable=ProgressBar)

    def test_ascii_dispatch(self):
        progress_ascii = ProgressBar(range(100), ascii_only=True)
        progress_non_ascii = ProgressBar(range(100), ascii_only=False)

        self.assertEqual(progress_ascii.bar_char, '=')
        self.assertEqual(progress_non_ascii.bar_char, '\u2588')

    def test_repr(self):
        bar = ProgressBar(10)
        self.assertEqual(repr(bar), 'ProgressBar(real_width = 10)')

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_first(self, mock_stdout):
        progress = ProgressBar(range(100), ascii_only=True)
        progress.bar_width = 50

        progress.print(step=1)

        self.assertEqual(mock_stdout.getvalue(), '\r1% | 001/100 | ')

    @patch('sys.stdout', new_callable=StringIO)
    def test_bar_iterator(self, mock_stdout):
        _ = mock_stdout
        test_iter = [step for step in ProgressBar(100, ascii_only=True)]

        self.assertEqual(test_iter, [step for step in range(100)])

    def test_int_arg(self):
        progress_range = ProgressBar(range(10))
        progress_int = ProgressBar(10)

        self.assertEqual(progress_range.iterable, progress_int.iterable)

    @patch('sys.stdout', new_callable=StringIO)
    def test_close(self, mock_stdout):
        progress = ProgressBar(range(100), ascii_only=True)
        progress.bar_width = 2

        progress.close()

        self.assertEqual(mock_stdout.getvalue(), '\r100% | 100/100 | == |\n')
