
import unittest
from unittest.mock import patch
from io import StringIO

from pyux.console import Speak


class TestWordy(unittest.TestCase):

    def test_assertion_noint(self):
        with self.assertRaises(TypeError):
            Speak(every=10.4)

    def test_assertion_zero(self):
        with self.assertRaises(ValueError):
            Speak(every=0)

    @patch('sys.stdout', new_callable=StringIO)
    def test_default_behavior(self, mock_stdout):
        for _ in Speak(range(5)):
            pass
        self.assertEqual(mock_stdout.getvalue(), '.....\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_int_range(self, mock_stdout):
        for _ in Speak(5):
            pass
        self.assertEqual(mock_stdout.getvalue(), '.....\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_every(self, mock_stdout):
        for _ in Speak(100, every=20):
            pass
        self.assertEqual(mock_stdout.getvalue(), '.....\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_newline(self, mock_stdout):
        for _ in Speak(100, every=20, newline=True):
            pass
        self.assertEqual(mock_stdout.getvalue(), '.\n.\n.\n.\n.\n')
