
import unittest
from unittest.mock import patch
from io import StringIO

from pyux.console import wordy


class TestWordyArgs(unittest.TestCase):
    def test_catch_wrong_length(self):
        with self.assertRaises(ValueError):
            @wordy(catch=True, colors=('a', 'b', 'c'))
            def nothing():
                return

    def test_nocatch_less1(self):
        with self.assertRaises(ValueError):
            @wordy(colors=())
            def nothing():
                return

    def test_nocatch_more1(self):
        with self.assertWarns(Warning):
            @wordy(colors=('RED', 'GREEN'))
            def nothing():
                return


class TestWordy(unittest.TestCase):

    @patch('sys.stdout', new_callable=StringIO)
    def test_default_values(self, mock_stdout):
        @wordy()
        def nothing():
            return
        [nothing() for _ in range(5)]
        self.assertEqual(mock_stdout.getvalue(), '.....')

    @patch('sys.stdout', new_callable=StringIO)
    def test_default_failure(self, mock_stdout):
        @wordy(catch=True)
        def error_4(x):
            if x == 4:
                raise ValueError
            return

        [error_4(x) for x in range(6)]
        self.assertEqual(mock_stdout.getvalue(), '....!.')

    @patch('sys.stdout', new_callable=StringIO)
    def test_verbose_failure(self, mock_stdout):
        @wordy(catch=True, message='success\n', failure_message='failure\n')
        def error_4(x):
            if x == 4:
                raise ValueError
            return

        [error_4(x) for x in range(6)]
        self.assertEqual(mock_stdout.getvalue(), 'success\n' *
                         4 + 'failure\n' + 'success\n')

    def test_failure(self):
        @wordy(catch=False)
        def error():
            raise ValueError

        with self.assertRaises(ValueError):
            error()
