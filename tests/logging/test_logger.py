
from io import StringIO
import os
import shutil
import unittest
from unittest.mock import patch

from pyux.logging import init_logger


class TestLogger(unittest.TestCase):

    def setUp(self):
        self.folder = 'test_logs'

    def tearDown(self):
        if os.path.exists(self.folder):
            shutil.rmtree(self.folder)

    @patch('logging.config')
    def test_read_default_config(self, mock_config):
        init_logger(folder=self.folder)
        self.assertTrue(mock_config.fileConfig.called)

    @patch('logging.config')
    def test_read_custom_config(self, mock_config):
        init_logger(config_file='logging.conf', folder=self.folder)
        self.assertTrue(mock_config.fileConfig.called)

    def test_log_filename(self):
        init_logger(
            folder=self.folder,
            filename='unittest',
            run_name='run',
            time_format='todelete'
        )
        self.assertTrue(os.path.exists('test_logs/unittest_run_todelete.log'))

    @patch('sys.stdout', new_callable=StringIO)
    def test_call_to_log(self, mock_write):
        target_str = 'This is an info\n'
        logger = init_logger(folder=self.folder)
        logger.info('This is an info')

        written = mock_write.getvalue().split(' :: ')[-1]
        self.assertEqual(written, target_str)

    @patch('logging.getLogger')
    @patch('logging.config')
    def test_existing_folder(self, mock_config, __):
        os.makedirs(self.folder, exist_ok=True)
        init_logger(folder=self.folder, filename='alone', run_name='test')
        self.assertTrue(mock_config.fileConfig.called)
