Logging
=======

Instantiate a logger
~~~~~~~~~~~~~~~~~~~~

The module contains a function ``init_logger`` that returns a logger
from the `logging package`_ with a fixed formatting, but choice in the log
file name. The default name contains the date and time of execution.

A different log file is created in the given folder at each code run, if the
default name for the log file is used. If you set an equal name
from one run to another, the various logs will be appended to the same log file.

``pyux`` comes with a default format for the logger, but you can specify
your own ``logging.conf``. Feel free to use ``ColourPen`` to color
logger messages :

.. code:: python

   from pyux.logging import init_logger
   from pyux.console import ColourPen
   from shutil import rmtree

   logger = init_logger(folder = 'logs', filename = 'activity', run_name = 'exemple', time_format = '%Y%m%d')
   pen = ColourPen

   # writes in green for debug
   pen.write(color = 'green')
   logger.debug('This ia a debug')

   # writes in red for critical
   pen.write(color = 'red', style = 'bright')
   logger.critical('This is a critical')

   # go back to normal for info
   pen.write(style = 'RESET_ALL')
   logger.info('This is an info')

   # rmtree('logs')

The same logger can be used throughout a whole project by calling
``logger = logging.getLogger(__name__)`` in submodules of the main script.

Detailed documentation
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyux.logging
   :members:
   :undoc-members:
   :show-inheritance:

.. _logging package: https://docs.python.org/3/howto/logging.html
