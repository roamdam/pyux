
.. image:: https://badge.fury.io/py/pyux-track.svg
   :target: https://badge.fury.io/py/pyux-track

.. image:: https://readthedocs.org/projects/pyux/badge/?version=latest
   :target: https://pyux.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status

.. image:: https://gitlab.com/roamdam/pyux/badges/master/pipeline.svg
   :target: https://gitlab.com/roamdam/pyux/commits/master
   :alt: Pipeline Status

.. image:: https://codecov.io/gl/roamdam/pyux/branch/master/graph/badge.svg
   :target: https://codecov.io/gl/roamdam/pyux

.. image:: https://api.codacy.com/project/badge/Grade/80fc156dcca544839a0b1643d31c9683    
   :target: https://www.codacy.com/manual/roamdam/pyux
   
----------------

.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Documentation

   console
   time
   logging
   errors

.. toctree::
   :maxdepth: 2
   :caption: Other content
   
   release_notes
   contributing

:ref:`genindex`
---------------
