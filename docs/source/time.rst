Time
====

This module contains classes that handle time : either measure it, wait
or even control it !

Timer
~~~~~

``Timer`` pause your script for the given number of seconds. With quite
the same design as wheel, you may add a message next to the timer.

.. code:: python

   from pyux.time import Timer

   # A timer for 3 seconds with a message
   Timer(delay = 3, message = 'Pausing 3 secs, tired')

   # A timer with no message
   Timer(delay = 3)

The timer can also be used as an iterable's decoration within a for statement,
when you repeatedly have to await the same delay at each iteration.
Specifying ``overwrite = True`` allows each iteration to be
rewritten on the same line, which is advised when used in that case.

Note that the first argument to ``Timer`` is ``iterable`` and not ``delay``, and
all of them have default values, so ``Timer(5)`` won't have the expected
behaviour !

By default, a timer in a for loop prints the iteration index next to the timer. 
Use ``pattern`` argument to specify a prefix to add to the default iteration
index (default to ``''``), or ``print_value`` to print the iteration
value rather than the index.

.. code:: python
   
   from pyux.time import Timer
   
   for fruit in Timer(['banana', 'apple', 'tomato'], delay = 3, print_value = True):
      pass

Again, the ``Timer.close()`` makes the counter go to zero *included* and flushes
a new line. It is called automatically when used as a loop decoration.

Wait
~~~~

Use ``wait`` to decorate a function that you want to pause for a given amount of
time before or after each execution. It can be useful for API calls in loops
where you have await a certain time between each API call. The pause can be made
before or after the function call, and ``Timer`` can be used instead of the built-in 
``sleep`` function with ``timer = True``. 

.. code:: python

   from pyux.time import wait

   @wait(delay = 3, before = True)
   def do_nothing():
      return
   do_nothing()


timeout
~~~~~~~

``timeout`` decorates a function that you want to timeout after a given delay.

.. code:: python

   from pyux.time import timeout

   @timeout(delay = 5)
   def unstopable_function():
      i = 0
      while i >= 0:
         i += 1
      return i

   unstopable_function()    # timeout stops execution after 5 seconds

Chronos
~~~~~~~

Use ``Chronos`` to measure user execution time, for a script or a loop.
It works as a stopwatch : rather than wrapping around and timing an
expression, it triggers at start, then the method ``Chronos.lap()``
records time with ``timeit.default_timer()`` each time it is called (thus
resembling a lap button on a stopwatch).

.. code:: python

   from time import sleep
   from pyux.time import Chronos

   chrono = Chronos()
   print('Step one with duration approx. 1.512 secs')
   sleep(1.512)
   chrono.lap(name = 'step 1')

   print('Step two with duration approx. 1.834 secs')
   sleep(1.834)
   chrono.lap(name = 'step 2')

   chrono.compute_durations(ms = True)

   print('\nNow printing durations :')
   for name, time in chrono.durations.items():
       print('Time for step %s : %d ms' % (name, time))

Durations can be written in a tsv file with ``Chronos.write_tsv()``.
The method uses an append mode, so you can append times from different runs 
to the same tracking file, for instance. Argument ``run_name`` in that method
allows you to give a name to a run especially for that purpose (the name appears
as the first column of the written file).

Three columns are written, with default names ``Execution`` (the one
presented just above), ``Step`` and ``Duration (secs)``. These names
can be changed with argument ``col_names``.

If you want to time iterations in a for loop, you can use it as a
decoration for the iterable. Since you won't be able to assign the
object back when the loop finishes, you can choose to print durations
in console, or write them into a tsv file.

.. code:: python

   from pyux.time import Chronos
   from pyux.time import Timer
   
   for index, value in enumerate(Chronos(range(1, 4), console_print = True, ms = True)):
      Timer(delay = value, message = 'At iteration %d' % index, overwrite = True)

Depending on the number of arguments you provide, declaration in the for
statement can become rather verbose. Feel free to initiate the chrono outside of the
loop, in which case, the object remains available after the loop (if you need to add
steps from the rest of the code afterwards, for instance).

.. code:: python

   from pyux.time import Chronos
   from time import sleep
   from os import unlink
   
   timed_iterable = Chronos(
      iterable = range(25),
      console_print = True,
      write_tsv = True,
      run_name = 'verbose_declaration',
      path = 'example_times.tsv',
      col_names = ('run', 'lap', 'duration (msecs)'),
      ms = True
   )
   for value in timed_iterable:
       sleep(value / 1000)
   # unlink('example_times.tsv')

Detailed documentation
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyux.time
   :members:
   :undoc-members:
   :show-inheritance:
