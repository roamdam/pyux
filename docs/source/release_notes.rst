Release notes
=============

Version 0.1.4
-------------

- all classes and functions can be imported from root ``from pyux import <target>``
- add codacy checks and flake8 enforcements

Version 0.1.3
-------------

- ``timeout`` decorates a function to make it stop after a given time of
  execution
- modules documentation were moved to their page instead of being in the
  ``README``

Version 0.1.2
-------------

- Both ``Timer`` and ``Wheel`` now have their decorating equivalent : ``wait``
  and ``wordy``
- ``wait`` decorates a function to make it pause before or after execution
- ``wordy`` decorates a function to make it say a message after execution, with
  the ability of catching exceptions
- ``Speak`` is the iterable version of ``wordy`` : applied to an iterable in
  a for loop, it prints a fixed message (or preferably a single character) at
  each iteration
- a ``contributing`` section was added in the documentation and the whole
  documentation and docstrings have been cleaned from some typos

Version 0.1.1
-------------

- ``Timer`` and ``Chronos``, even if it is less expected from
  them, can be used as for loop's decorations too
- Documentation was improved : both docstrings, readthedocs
  structure and README
- The demonstration was updated to be more efficient in presenting
  the available tools
- Pipelines set up on GitLab so that ``develop`` version is always
  available on readthedocs

Version 0.1.0
-------------

- First version published on PyPi
- ``ColourPen`` has a standalone ``write`` method, only this one
  can be used to colourise different sentences
- ``Wheel`` and ``ProgressBar`` can be used as for loop's decorations,
  avoiding to manually declare instantiation, printing and closing