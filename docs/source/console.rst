Console
=======

This module contains classes that print things to the terminal during
execution. Most of them are meant to be used as decorators of a for
statement, where they can help track where you are in the loop.

You can also use them manually to customise behaviour within a loop
or anywhere else in your scripts.

Wheel
~~~~~

Use ``Wheel`` as an iterable’s decoration to have a turning *wheel*
printed during a ``for`` loop, turning as fast as the iterations go. By
default the iteration index is printed next to the wheel, but you can
choose to print the iteration value instead of the iteration index with
argument ``print_value = True``.

When used over a standard ``range(n)``, it accepts ``n`` as an argument
for the iterable.

.. code:: python

   from pyux.console import Wheel
   from time import sleep
   
   myrange = 'An iterable string'
   for step in Wheel(myrange, print_value = True):
      sleep(0.2)
   
   for step in Wheel(20):
      sleep(0.1)

If you must, you can also instantiate ``Wheel`` outside of a loop then
trigger it manually. It is useful mainly to customise the message you
want to print.

Note that when using it manually, you may want to *close* the wheel when
you're done : it prints the length of the iterable or a message if provided,
and flushes a new line. Otherwise the console cursor is still on the
previous line. When decorating a for loop, it is automatically called when
the loop finishes.

.. code:: python

   from time import sleep
   from pyux.console import Wheel 

   wheel = Wheel()                  # no need for an iterator argument in that case
   for index, word in enumerate(['coffee', 'vanilla', 'cocoa']):
       wheel.print(step = index, message = 'Running on word %s            ' % word)
       sleep(1)                  # renders best if iterations are not too fast
   wheel.close(message = 'No more words to think about.')

``Wheel`` can also be used to decorate a generator object rather than an iterable,
which is especially useful with ``os.walk()``, for instance, to know both if
your script is still running and the total number of read paths. This also makes
it compatible with ``enumerate()``.

Speak and wordy
~~~~~~~~~~~~~~~

``wordy`` is a decorator function that prints a message at each call of the 
decorated function. You can catch exceptions from the decorated function with
``catch = True`` and print a specific message when the exception is catch.

By default, ``.`` is printed if the call is successful, and ``!`` if it isn't,
allowing easy and concise error tracking within loops. When catching an exception,
the exception is returned in place of the function response.

.. code:: python

   from pyux.console import wordy

   @wordy(catch = True, failure_message = 'x')
   def error_4(x):
       if x == 4:
           raise ValueError
   [error_4(x) for x in range(10)]

Class ``Speak`` does roughly the same as ``wordy``, but decorates an iterable 
in a for loop rather than a function. It thus prints a message at each iteration or
every given number of iterations. It does not provide any exception catching
environment. For that, you'll have to decorate the function which you want to
catch from with ``wordy`` instead of using ``Speak``.

As for all other iterable's decorators in this package, standard ``range(n)``
iterables can be given with just ``n`` as the ``iterable`` argument.

.. code:: python

   from pyux.console import Speak

   for x in Speak(45, every = 5):
       pass

Both ``wordy`` and ``Speak`` print their messages with ``sys.stdout.write()``, 
and not ``print()``, so that the console cursor can stay on the same line within
the same loop. When using ``Speak`` on an iterable in a for loop, a ``close()``
method is automatically called when the loop finished to flush a new line. When
used *manually*, you'll have to flush ``\n`` (or ``print('')``) to make the
cursor go to the new line.

ProgressBar
~~~~~~~~~~~

Use ``ProgressBar`` as an iteratable’s decoration to have a progress bar
printed during a ``for`` loop. It mimics, in a extremely simplified way,
the behavior of the great `tqdm`_ progress bar. As for ``Wheel``, you
can also use it manually.

Integers for ``iterable`` argument are read as ``range(n)``.

.. code:: python

   from pyux.console import ProgressBar
   from time import sleep
   
   for index in ProgressBar(2500):     
       sleep(0.001)

Since ``ProgressBar`` needs to know at initialisation the total number
of iterations (to calculate the bar's width and the percentage),
it is not usable with generators. A workaround is to give it an approximate
number of iterations as ``iterable`` argument, and use it manually. Careful
though, should the approximation be too short, the bar will expand further
than the console width (or never reach 100% if too big).

.. code:: python

   from pyux.console import ProgressBar
   from time import sleep
   
   def simple_generator(n):
      for i in range(n):
         yield i
   
   bar = ProgressBar(1000)
   for value in simple_generator(1200):
      sleep(0.002)
      bar.print(step = value)
   print('Too long !')
   
   bar.current_console_step = 0    # resetting bar to zero
   for value in simple_generator(800):
       sleep(0.002)
       bar.print(step = value)
   print('Too short !')

A *manual* ProgressBar can also be used to track progression on scripts
with distinct stages that are not necessarily in the form of a loop,
should there be so many of them that it makes sense.

Do use ``ProgressBar.close()`` method to be sure that the 100% iteration
will be printed and the console cursor flushed to a new line when you
use it manually. When decorating a for statement, it is automatically
called when the loop finishes.

.. code:: python

   bar = ProgressBar()

   # here goes stage 1
   bar.print(step = 1)

   # here goes ...
   bar.print(step = ...)

   bar.close()

ColourPen
~~~~~~~~~

Use ``ColourPen`` as its name indicates : to write colored text in
terminal. It uses ``colorama`` package. Use a single instance for
different styles thanks to the possibility of chaining ``write``
instructions.

.. code:: python

   from pyux.console import ColourPen

   pen = ColourPen()
   pen.write('Hello', color = 'cyan', style = 'normal')\
       .write(' this is another', color = 'red')\
       .write(' color on the same line.', color = 'green', newline = True)\
       .write("The same color on a new line. I'm reseting styles after that.", newline = True, reset = True)\
       .write('A normal goodbye.')\
       .close()

``ColourPen.close()`` resets styles to normal, flushes to a new line and closes
``colorama``, which means that if you do not initialise a pen instance again,
the colouring and styling won't work anymore.

Detailed documentation
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyux.console
   :members:
   :undoc-members:
   :show-inheritance:

.. _tqdm: https://github.com/tqdm/tqdm
